// Import the necessary TailwindCSS plugin
import forms from '@tailwindcss/forms';

export default {
  content: [
    './public/**/*.html',
    './src/**/*.{js,jsx,ts,tsx}',
  ],
  mode: 'jit', // Just-In-Time mode
  darkMode: 'media', // or 'class' - enables dark mode based on media or class
  theme: {
    fontFamily: {
      display: ['Open Sans', 'sans-serif'],
      body: ['Open Sans', 'sans-serif'],
    },
    extend: {
      screens: {
        mf: '990px', // Custom breakpoint
      },
      keyframes: {
        'slide-in': { // Custom animation
          '0%': {
            transform: 'translateX(120%)',
          },
          '100%': {
            transform: 'translateX(0%)',
          },
        },
      },
      animation: {
        'slide-in': 'slide-in 0.5s ease-out',
      },
    },
  },
  variants: {
    extend: {}, // Here you can extend your variants
  },
  plugins: [
    forms, // Integrate the @tailwindcss/forms plugin
  ],
};
