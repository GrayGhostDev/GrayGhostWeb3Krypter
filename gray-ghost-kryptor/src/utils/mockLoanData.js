// mockLoanData.js
export default [
  {
    loanId: 1,
    borrowerId: 123,
    amount: 1000,
    interestRate: 5.5,
    startDate: "2021-12-01",
    dueDate: "2022-12-01",
    status: "Active",
  },
  {
    loanId: 2,
    borrowerId: 124,
    amount: 2000,
    interestRate: 6.0,
    startDate: "2021-06-01",
    dueDate: "2022-06-01",
    status: "Completed",
  },
  // Additional mock loans
];
