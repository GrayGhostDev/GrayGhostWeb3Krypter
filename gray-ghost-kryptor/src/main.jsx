import React from "react";
import ReactDOM from "react-dom";
import {ThirdwebProvider} from "@thirdweb-dev/react";  // Import the ThirdwebProvider
import App from "./App.jsx";
import {TransactionsProvider} from "./context/TransactionContext";
import "./index.css";
import * as Buffer  from "buffer";
import {DevSupport} from "@react-buddy/ide-toolbox";
import {ComponentPreviews, useInitial} from "/src/dev/index.ts";

// Choose the chainId that corresponds to the network you want to interact with
const desiredChainId = 1; // For Ethereum Mainnet; change as needed for other networks

// Ensure the desiredChainId is passed as a prop to the ThirdwebProvider
window.Buffer = Buffer;

ReactDOM.render(
    <React.StrictMode>
        <ThirdwebProvider desiredChainId={1}>
            <TransactionsProvider>
                <DevSupport ComponentPreviews={ComponentPreviews}
                            useInitialHook={useInitial}
                >
                    <App/>
                </DevSupport>
            </TransactionsProvider>
        </ThirdwebProvider>
    </React.StrictMode>,
    document.getElementById("root")
);
