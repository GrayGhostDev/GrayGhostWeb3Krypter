import React, { useState, useContext } from 'react';
import { TransactionContext } from '../context/TransactionContext';
import useSendTransaction from '../hooks/useSendTransaction';

const SendEtherForm = () => {
    const [recipient, setRecipient] = useState('');
    const [amount, setAmount] = useState('');
    const sendTransaction = useSendTransaction();

    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
            const receipt = await sendTransaction({
                to: recipient,
                value: amount,
                data: ""  // This would be used if sending data to a contract function
            });
            console.log('Transaction successful:', receipt);
        } catch (error) {
            console.error('Transaction failed:', error);
        }
    };

    return (
        <form onSubmit={handleSubmit}>
            <input
                type="text"
                value={recipient}
                onChange={(e) => setRecipient(e.target.value)}
                placeholder="Recipient Address"
                required
            />
            <input
                type="text"
                value={amount}
                onChange={(e) => setAmount(e.target.value)}
                placeholder="Amount in ETH"
                required
            />
            <button type="submit">Send Ether</button>
        </form>
    );
};

export default SendEtherForm;
