import { useEffect, useState } from "react";
import axios from 'axios';

// Example environment variables (these should be securely stored and not exposed in your codebase)
const PUBLIC_KEY = import.meta.env.VITE_LOANDISK_PUBLIC_KEY;
const BRANCH_ID = import.meta.env.VITE_LOANDISK_BRANCH_ID;
const AUTH_CODE = import.meta.env.VITE_LOANDISK_AUTH_CODE;

const useFetch = ({ borrowerId }) => {
  const [borrowerData, setBorrowerData] = useState(null);

  const fetchBorrower = async () => {
    const url = `https://api-main.loandisk.com/${PUBLIC_KEY}/${BRANCH_ID}/borrower/${borrowerId}`;

    try {
      const response = await axios.get(url, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Basic ${AUTH_CODE}`
        }
      });

      if (response.data && response.data.response) {
        setBorrowerData(response.data.response);
      } else {
        throw new Error('Failed to fetch data');
      }
    } catch (error) {
      console.error('Error fetching borrower details:', error);
      setBorrowerData(null);
    }
  };

  useEffect(() => {
    if (borrowerId) {
      fetchBorrower();
    }
  }, [borrowerId]);

  return borrowerData;
};

export default useFetch;
