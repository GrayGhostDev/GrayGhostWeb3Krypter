import React from 'react';
import { useConnect } from '@thirdweb-dev/react';
import { createWallet, injectedProvider } from '@thirdweb-dev/sdk';

const WalletConnect = () => {
  const { connect, isConnecting, error } = useConnect();

  const handleConnect = async (walletId) => {
    try {
      await connect(async () => {
        const wallet = createWallet(walletId);
        const isInjected = await injectedProvider(walletId);
        if (isInjected) {
          await wallet.connect();
        } else {
          await wallet.connect({ showQrModal: true });
        }
        return wallet;
      });
    } catch (err) {
      console.error('Failed to connect wallet:', err);
    }
  };

  return (
    <div>
      {error && <p>Error: {error.message}</p>}
      {['io.metamask', 'io.coinbase.wallet', 'io.trustwallet', 'io.bitcoin.wallet'].map(walletId => (
        <button
          key={walletId}
          onClick={() => handleConnect(walletId)}
          disabled={isConnecting}
          style={{ margin: '10px', padding: '10px' }}
        >
          {isConnecting ? `Connecting to ${walletId}...` : `Connect ${walletId}`}
        </button>
      ))}
    </div>
  );
};

export default WalletConnect;
