// useAccountBalance.jsx
import { useState, useEffect } from 'react';
import { ethers } from 'ethers';

const useAccountBalance = (account) => {
  const [balance, setBalance] = useState("0");

  useEffect(() => {
    const fetchBalance = async () => {
      if (account && window.ethereum) {
        const provider = new ethers.providers.Web3Provider(window.ethereum);
        const balance = await provider.getBalance(account);
        setBalance(ethers.utils.formatEther(balance));
      }
    };

    fetchBalance();
  }, [account]);

  return balance;
};

export default useAccountBalance;
