import { useState, useEffect } from 'react';
import axios from 'axios';
import { loandiskAPIBaseURL, loandiskAuthCode, loandiskPublicKey, loandiskBranchId } from "../utils/constants.js";

function useFetchLoanDisk(resource, borrowerId, transactionTypeId) {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`${loandiskAPIBaseURL}/${loandiskPublicKey}/${loandiskBranchId}/${resource}`, {
          params: {
            borrowerId: borrowerId,
            transactionTypeId: transactionTypeId
          },
          headers: { Authorization: `Basic ${loandiskAuthCode}` }
        });
        setData(response.data);
      } catch (error) {
        setError(error);
      } finally {
        setLoading(false);
      }
    };

    fetchData().then(r => r.json());
  }, [resource, borrowerId, transactionTypeId]);

  return { data, loading, error };
}

export default useFetchLoanDisk;
