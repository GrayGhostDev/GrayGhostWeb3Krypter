// useSmartContract.jsx
import { useContext } from 'react';
import { TransactionContext } from '../context/TransactionContext';

const useSmartContract = (contractABI, contractAddress) => {
  const { sdk } = useContext(TransactionContext);

  const contract = new sdk.ethers.Contract(contractAddress, contractABI, sdk.getSigner());

  return contract;
};

export default useSmartContract;
