// src/App.jsx

import React from 'react';
import { TransactionsProvider } from "./context/TransactionContext";
import { Navbar, Welcome, Services, Transactions, Footer } from "./components";

const App = () => (
  <TransactionsProvider>
    <div className="min-h-screen">
      <div className="gradient-bg-welcome">
        <Navbar />
        <Welcome />
      </div>
      <Services />
      <Transactions />
      <Footer />
    </div>
  </TransactionsProvider>
);

export default App;
