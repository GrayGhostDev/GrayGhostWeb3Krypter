import React, { createContext, useState, useEffect } from 'react';
import { ethers } from 'ethers';
import { contractAddress, contractABI, sdk } from '../utils/constants'; // Ensure sdk is properly initialized and exported in constants.

export const TransactionContext = createContext();

// Helper function to create a new Ethereum contract instance
const createEthereumContract = () => {
  const signer = sdk.getSigner();
  return new ethers.Contract(contractAddress, contractABI, signer);
};

export const TransactionsProvider = ({ children }) => {
  const [currentAccount, setCurrentAccount] = useState(null);
  const [savingTransactions, setSavingTransactions] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  // Function to connect to the user's wallet
  const connectWallet = async () => {
    try {
      const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
      setCurrentAccount(accounts[0]);
    } catch (error) {
      console.error("Error connecting to MetaMask:", error);
    }
  };

  // Function to fetch saving transactions from LoanDisk API
  const fetchSavingTransactions = async () => {
    if (!currentAccount) return;
    setIsLoading(true);
    try {
      const response = await fetch(`https://api-main.loandisk.com/${process.env.LOANDISK_PUBLIC_KEY}/${process.env.LOANDISK_BRANCH_ID}/saving`, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Basic ${process.env.LOANDISK_AUTH_CODE}`
        }
      });
      const data = await response.json();
      setSavingTransactions(data.results || []); // Adjust according to the actual response structure
    } catch (error) {
      console.error("Failed to fetch saving transactions:", error);
    } finally {
      setIsLoading(false);
    }
  };

  // Check if user is already connected when component mounts
  useEffect(() => {
    const checkIfWalletIsConnected = async () => {
      try {
        const accounts = await window.ethereum.request({ method: 'eth_accounts' });
        if (accounts.length > 0) {
          setCurrentAccount(accounts[0]);
        }
      } catch (error) {
        console.error("Failed to check wallet connection:", error);
      }
    };

    checkIfWalletIsConnected();
  }, []);

  // Fetch saving transactions whenever currentAccount changes and is truthy
  useEffect(() => {
    if (currentAccount) {
      fetchSavingTransactions();
    }
  }, [currentAccount]);

  return (
    <TransactionContext.Provider value={{
      currentAccount,
      savingTransactions,
      connectWallet,
      isLoading,
      fetchSavingTransactions
    }}>
      {children}
    </TransactionContext.Provider>
  );
};

export default TransactionsProvider;
