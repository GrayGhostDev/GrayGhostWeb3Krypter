import React, { useState, useContext } from 'react';
import { TransactionContext } from '../context/TransactionContext';

function LoanDiskForm() {
  const [borrowerId, setBorrowerId] = useState('');
  const [showSavings, setShowSavings] = useState(false);
  const { fetchBorrowerData, fetchSavingsTransactions } = useContext(TransactionContext);

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (showSavings) {
      await fetchSavingsTransactions(borrowerId);  // Assumes this function is implemented in context
    } else {
      await fetchBorrowerData(borrowerId);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Borrower ID:
        <input type="text" value={borrowerId} onChange={(e) => setBorrowerId(e.target.value)} />
      </label>
      <label>
        Show Savings Transactions:
        <input type="checkbox" checked={showSavings} onChange={() => setShowSavings(!showSavings)} />
      </label>
      <button type="submit">Fetch Information</button>
    </form>
  );
}

export default LoanDiskForm;
