import React, { useState } from 'react';

const AssetTransferForm = () => {
    const [formData, setFormData] = useState({
        assetId: '',
        amount: '',
        recipient: '',
        message: ''
    });
    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState('');

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        setLoading(true);
        setMessage('');

        // Basic validation
        if (!formData.recipient || !formData.amount || isNaN(formData.amount) || parseFloat(formData.amount) <= 0) {
            setMessage("Please ensure all fields are correctly filled and amount is positive.");
            setLoading(false);
            return;
        }

        try {
            console.log("Submitting form", formData);
            // Simulate an API call
            await new Promise(resolve => setTimeout(resolve, 2000)); // Mock delay for the API call

            // If the API call was successful, update the UI accordingly
            setMessage('Transaction successful!');
            setFormData({
                assetId: '',
                amount: '',
                recipient: '',
                message: ''
            });
        } catch (error) {
            // Handle errors if the API call fails
            setMessage('Transaction failed: ' + error.message);
        }
        setLoading(false);
    };

    return (
        <div>
            {message && <p>{message}</p>}
            <form onSubmit={handleSubmit}>
                <label htmlFor="assetId">Asset ID:</label>
                <input type="text" name="assetId" value={formData.assetId} onChange={handleChange} />

                <label htmlFor="amount">Amount:</label>
                <input type="number" name="amount" value={formData.amount} onChange={handleChange} />

                <label htmlFor="recipient">Recipient Address:</label>
                <input type="text" name="recipient" value={formData.recipient} onChange={handleChange} />

                <label htmlFor="message">Message (Optional):</label>
                <input type="text" name="message" value={formData.message} onChange={handleChange} />

                <button type="submit" disabled={loading}>{loading ? 'Processing...' : 'Transfer Asset'}</button>
            </form>
        </div>
    );
};

export default AssetTransferForm;
