import react from "react";
import { useConnect } from "../hooks/useConnect";// Ensure it's a hook

const WalletConnect = () => {
  const { connectWallet, currentAccount } = useConnect();

  return (
    <div>
      <h1>Connect Your Wallet</h1>
      {currentAccount ? <p>Connected: {currentAccount}</p> : <button onClick={connectWallet}>Connect</button>}
    </div>
  );
};

export default WalletConnect;
