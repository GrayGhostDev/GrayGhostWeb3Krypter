import React, { useState } from 'react';
import useContract  from '../hooks/useContract';  // Corrected import statement

const AssetTokenization = () => {
    const [assetDetails, setAssetDetails] = useState({});
    const { tokenizeAsset } = useContract();  // Custom hook to call Thirdweb tokenization

    const handleSubmit = async (event) => {
        event.preventDefault();
        const result = await tokenizeAsset(assetDetails);
        console.log(result);
    };

    // Form to input asset details and call handleSubmit
    return (
        <form onSubmit={handleSubmit}>
            {/* Form fields for asset details */}
            <button type="submit">Tokenize Asset</button>
        </form>
    );
};

export default AssetTokenization;
