import React from "react";
import { BsShieldFillCheck } from "react-icons/bs";
import { BiSearchAlt } from "react-icons/bi";
import { RiHeart2Fill, RiBankLine } from "react-icons/ri";
import { MdAccountBalanceWallet } from "react-icons/md";

const ServiceCard = ({ color, title, icon, subtitle }) => (
  <div className="flex flex-row justify-start items-start white-glassmorphism p-3 m-2 cursor-pointer hover:shadow-xl">
    <div className={`w-10 h-10 rounded-full flex justify-center items-center ${color}`}>
      {icon}
    </div>
    <div className="ml-5 flex flex-col flex-1">
      <h3 className="mt-2 text-white text-lg">{title}</h3>
      <p className="mt-1 text-white text-sm md:w-9/12">
        {subtitle}
      </p>
    </div>
  </div>
);

const Services = () => (
  <div className="flex w-full justify-center items-center gradient-bg-services">
    <div className="flex mf:flex-row flex-col items-center justify-between md:p-20 py-12 px-4">
      <div className="flex-1 flex flex-col justify-start items-start">
        <h1 className="text-white text-3xl sm:text-5xl py-2 text-gradient ">
          Comprehensive Web3 Services
          <br />
          Empower Your Transactions
        </h1>
        <p className="text-left my-2 text-white font-light md:w-9/12 w-11/12 text-base">
          Connect, transact, and innovate with our platform's integrations for Thirdweb and LoanDisk.
        </p>
      </div>

      <div className="flex-1 flex flex-col justify-start items-center">
        <ServiceCard
          color="bg-[#2952E3]"
          title="Secure Wallet Connection"
          icon={<MdAccountBalanceWallet fontSize={21} className="text-white" />}
          subtitle="Connect seamlessly to blockchain wallets using Thirdweb SDK."
        />
        <ServiceCard
          color="bg-[#8945F8]"
          title="Loan and Saving Transactions"
          icon={<RiBankLine fontSize={21} className="text-white" />}
          subtitle="Access and manage your financial data from LoanDisk efficiently."
        />
        <ServiceCard
          color="bg-[#F84550]"
          title="Asset Tokenization"
          icon={<BiSearchAlt fontSize={21} className="text-white" />}
          subtitle="Tokenize real-world assets quickly and securely on the blockchain."
        />
        <ServiceCard
          color="bg-green-500"
          title="Fastest Blockchain Interactions"
          icon={<RiHeart2Fill fontSize={21} className="text-white" />}
          subtitle="Enjoy quick, reliable blockchain transactions with optimized smart contract interactions."
        />
      </div>
    </div>
  </div>
);

export default Services;
