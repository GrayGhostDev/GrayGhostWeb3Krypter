// SPDX-License-Identifier: MIT
pragma solidity ^0.8.9;

import "@chainlink/contracts/src/v0.8/ChainlinkClient.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "../providers/ChainLink/chainlink/contracts/src/v0.8/ChainlinkClient.sol";

contract SavingsDataBridge is ChainlinkClient, Ownable {
    using Chainlink for Chainlink.Request;

    uint256 private fee;
    bytes32 private jobId;
    address private oracle;

    struct SavingsTransaction {
        uint256 transactionId;
        uint256 amount;
        string transactionType; // 'deposit', 'withdrawal', etc.
    }

    uint256 private nextTransactionId;
    mapping(uint256 => SavingsTransaction) public transactions;

    event TransactionRecorded(uint256 transactionId, uint256 amount, string transactionType);

    // Constructor
    constructor(address _oracle, string memory _jobId, uint256 _fee, address _link) {
        setChainlinkToken(_link);
        oracle = _oracle;
        jobId = stringToBytes32(_jobId);
        fee = _fee;
        nextTransactionId = 1;
    }

    // Record a new savings transaction
    function recordTransaction(uint256 amount, string memory transactionType) public onlyOwner {
        uint256 transactionId = nextTransactionId++;
        transactions[transactionId] = SavingsTransaction(transactionId, amount, transactionType);
        emit TransactionRecorded(transactionId, amount, transactionType);
    }

    // Helper function to convert string to bytes32
    private pure function stringToBytes32(string memory source) returns (bytes32 result) {
        bytes memory tempEmptyStringTest = bytes(source);
        if (tempEmptyStringTest.length == 0) {
            return 0x0;
        }
        assembly {
            result := mload(add(source, 32))
        }
    }
}
