require('@nomiclabs/hardhat-waffle');
require('dotenv').config(); // Import dotenv to use environment variables

module.exports = {
  defaultNetwork: 'sepolia', // Set default network to Sepolia for testing
  networks: {
    hardhat: {
      chainId: 31337
    },
    sepolia: { // Test environment using Sepolia
      url: process.env.ALCHEMY_SEPOLIA_URL,
      accounts: [
        process.env.PRIVATE_KEY1,
        process.env.PRIVATE_KEY2
      ].filter(key => key != null) // Filter out undefined keys
    },
    ethereum: { // Production environment using Ethereum mainnet
      url: process.env.ALCHEMY_ETHEREUM_URL,
      accounts: [
        process.env.PRIVATE_KEY1,
        process.env.PRIVATE_KEY2
      ].filter(key => key != null) // Filter out undefined keys
    }
  },
  solidity: {
    version: '0.8.29',
    settings: {
      optimizer: {
        enabled: true,
        runs: 200
      }
    }
  },
  paths: {
    sources: './contracts',
    tests: './test',
    cache: './cache',
    artifacts: './artifacts'
  },
  mocha: {
    timeout: 40000
  }
};
