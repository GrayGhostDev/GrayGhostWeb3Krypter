import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    // Specify extensions explicitly to handle .cjs and others
    extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.cjs']
  },
  server: {
    port: 5143
  },
  css: {
    postcss: {
      plugins: [
        require('tailwindcss'),
        require('autoprefixer')
      ]
    }
  }
});
