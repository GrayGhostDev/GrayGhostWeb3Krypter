// Use CommonJS syntax to import the necessary PostCSS plugins
const autoprefixer = require('autoprefixer');
const tailwindcss = require('tailwindcss');

module.exports = {
  plugins: [
    tailwindcss('./tailwind.config.ts'),
    autoprefixer
  ]
};
